/*um zu schauen ob ein string korrekt geklammert ist*/
/*Note: korrekt geklammert ist, wenn es genau so viel klammern auf wie zu hat.
Technically auch, wenn der String leer ist, aber der Spezialfall wird hier ignoriert*/
#include <stdio.h>
#include <string.h>

int main () {
/*get the input*/
char str[1000];
printf("Geben Sie einen zu pruefenden String ein: \n");
fgets(str,sizeof(str),stdin);
int correct = 1;

/*fangen bei vollem string an und gehen sukzessive nach innen vor*/
/*Note: das schaut sich die werte in den ascii-codes an, deswegen checke ich auch 
    in ascii-codes, ob es passt: 10 entspricht neue Zeile 
    (immer das letzte im String), 40 ist (, und 41 )*/
while (strlen(str) > 1) //leerer string hat laenge 1
{
    if (str[0] == 41) //string starts with closing bracket
    {
        correct = 0;
        break;
    }
    else {
        if (str[0] == 40)
        {
            //find the matching closing bracket
            int ii = 1;
            int matched = 0;
            int index = 0;

            while (ii < strlen(str))
            {
                if (str[ii] == 41)
                {
                    matched = 1;
                    index = ii;
                    break;
                }
                ii += 1;
            }
            if (matched == 1)
            {
                
                for (int jj = index; jj < strlen(str)-1; jj++)
                {
                    str[jj] = str[jj+1]; //schieben sie alle eins nach links
                }
                str[strlen(str)-1] = * "\0"; //letzter eintrag weg, weil geloescht
                //jetzt noch den ersten loswerden
                for (int kk = 0; kk < strlen(str)-1; kk++)
                {
                    str[kk] = str[kk+1];
                }
                str[strlen(str)-1] = * "\0"; //wieder den letzten loswerden
            } else {
                correct = 0;
                break;
            }
        }
        else {
            correct = 0;
            break; //weil da etwas drin war, was keine klammer ist
        }
    }
}

if (correct == 1)
{
    printf("String ist korrekt geklammert \n");
} else
{printf("String ist nicht korrekt geklammert \n");}

}
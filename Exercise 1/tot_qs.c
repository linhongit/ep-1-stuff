/*fuer totale quersumme einer zahl*/
#include <stdio.h>

int main () {
    int x;
    printf("Geben Sie eine natuerliche Zahl ein \n");
    scanf("%d", &x);

    /*wiederhole es so lange, bis tot QS zwischen 0 und 9*/
    int qs = 1000; /*just some initial setup*/
    int cur_num = x; /*for the number we take the qs from*/
    while (qs > 9)
    {
        /*starte bei leerer quersumme, addiere jeweils die stellen dazu*/
        qs = 0;
        int power = 1;

        /*finde hoechsten stellenwert raus*/
        while (cur_num > power)
        {
            power *= 10;
        }
        if (cur_num != power)
        {
            power /= 10;
        
        }
       

        /*extrahiere ziffern*/
        while (cur_num != 0)
        {
            int digit = cur_num / power;
            cur_num = cur_num - digit*power;
            printf("%d\n", digit);
            qs = qs + digit;
            if (power != 1)
            {
                power /= 10;
            }
        }
        cur_num = qs;
    }
    printf("Totale Quersumme von n ist %d\n",qs);

}
#include <stdio.h>
#include <math.h>

int main () {
   /*Erzeugung eines pythagoreischen Tripels: c = m^2 + n^2 f beliebige m,n, m>n*/
    int x;

    printf("Geben Sie eine beliebige natuerliche Zahl ein\n");
    scanf("%d", &x);

    int num = 0;
    int ii = 1;
    
    while (ii<x)
    {
        int jj = 1;
        while (((pow(ii,2)+pow(jj,2))<=x) && (jj<ii))
        {
            
            num = num + 1;
            jj = jj + 1;
        }
        ii = ii + 1;
    }

    printf("Anzahl an Tripel f. Bedingung = %d\n", num);
    return num;

}
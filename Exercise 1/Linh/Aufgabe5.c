/**
 * @author Linh Pham
 * @version 24.10.23
*/

#include <stdio.h>
#include <string.h>

int main () {
    char s[100];
    int val = 0;
    int i = 0;

    printf("Insert string: \n");
    scanf("%s", s);

    while (val >= 0){   
        if (s[i] == '(') {
            val += 1;
        }
        else if (s[i] == ')') {
            val -= 1;            
        }
        if (i < strlen(s)) {
            i++;
        } else {
            break;
        }
    }

    if (val == 0){
        printf("korrekt geklammert\n");
    } else {
        printf("falsch\n");
    }
    
    return 0;
}

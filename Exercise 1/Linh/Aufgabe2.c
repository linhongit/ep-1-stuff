/**
 * @author Linh Pham
 * @version 23.10.23
*/

#include <stdio.h>

int main () {
    int i, j, k;

    printf("Insert three integer number i, j and k: \n");
    scanf("%d %d %d", &i, &j, &k);

    printf("Numbers between %d and %d divisible by %d (in descending order): \n", i, j, k);
    // x descending from j to i, check if x divisible by k  
    for (int x = j; x >= i; x--) {
        if (x % k == 0) {
            printf("%d\n", x);
        }
    }

    return 0;
}

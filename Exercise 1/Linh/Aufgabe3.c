/**
 * @author Linh Pham
 * @version 23.10.23
*/

#include <stdio.h>

int main () {
    int n;
    // Counter i
    int i = 0;
    
    printf("Insert an integer: \n");
    scanf("%d", &n);

    // Increment from 1 to n with a<b<c<=n 
    for (int a = 1; a < n; a++) {
        for (int b = a + 1; b < n; b++) {
            for (int c = b + 1; c <= n; c++) {
                if (c * c == a * a + b * b) {
                    // Add 1 to counter if a, b, c is a pythagorean triple
                    i++;
                }
            }
        }
    }

    printf("Number of Pythagorean triples: %d \n", i);

    return 0;
}

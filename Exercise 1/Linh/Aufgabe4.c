/**
 * @author Linh Pham
 * @version 23.10.23
*/

#include <stdio.h>

int sumdigit(int x) {
    int sum = 0;
    
    // Sum the digits up from right to left
    while (x > 0){
        int last_digit = x % 10;
        sum += last_digit;
        x = (x - last_digit) / 10;
    }

    return sum;
}

int main () {
    int n;

    printf("Insert an integer: \n");
    scanf("%d", &n);

    int TQ = sumdigit(n);

    // Sum up until TQ is a single-digit sum
    while (TQ > 9) {
        TQ = sumdigit(TQ);
    }

    printf("TQ(%d) = %d \n", n, TQ);

    return 0;
}


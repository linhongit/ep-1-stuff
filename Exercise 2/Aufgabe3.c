/**
 * @author Linh Pham
 * @version 07.11.23
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
    srand(time(NULL));
    int N;
    int t = 0;
    printf("Please enter N: \n");
    scanf("%d",&N);
    
    double x, y;
    for (int i = 0; i<N; ++i){
        x = (double) rand() / RAND_MAX;
        y = (double) rand() / RAND_MAX;
        if ((x*x+y*y)<=1) {
            t +=1;
        }
    }
    double pi = 4.0* (double) t / (double) N;
    printf("pi = %f\n", pi);
    
}

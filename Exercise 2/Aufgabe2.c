/**
 * @author Linh Pham
 * @version 07.11.23
*/

#include <stdio.h>
#include <stdlib.h>
#include "Aufgabe1.h"

int main () {
    // Fractions a/b and c/d
    int a, b, c, d;
    // Result sum1/sum2
    int sum1, sum2;
    printf("Insert the first fraction: \n");
    scanf("%d %d", &a, &b);
    printf("Insert the second fraction: \n");
    scanf("%d %d", &c, &d);

    if (b*d == 0){
        printf("Denominator may not be 0");
        return 0;
    }
    // Sum up in the form a*d/b*d + c*b/d*b
    sum1 = a*d+c*b;
    sum2 = b*d;
    // Simplify
    int ggt;
    ggt = euclid(abs(sum1), abs(sum2));
    sum1 /= ggt;
    sum2 /= ggt;

    printf("%d %d + %d %d = %d %d \n", a, b, c, d, sum1, sum2);
}



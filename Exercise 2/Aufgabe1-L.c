/**
 * @author Linh Pham
 * @version 07.11.23
*/

#include <stdio.h>
#include "Aufgabe1.h"

int main () {
    int a, b;
    int ggt;

    printf("Please enter two integers: \n");
    scanf("%d %d", &a, &b);

    ggt = euclid(a, b);
    printf("ggt = %d\n", ggt);
}

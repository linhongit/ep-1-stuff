#include "mat_mult.h"
//N is defined as 4


/* Writes the product of a nxm matrix A, given as a flat (1-dimensional) array of length n*m
   as specified on the exercise sheet.
   and a mxo matrix B, given as a flat array of length m*o
   to the array C
*/
void mat_mult(int n, int m, int o, int A[], int B[],int C[]) {
    //a_ij in A[i*n+j]
    for (int i=0; i<n;i++){
        for (int j=0; j<o; j++){
            C[i*n+j] = 0;
            for (int k=0; k<m; k++){
                C[i*n+j] += A[i*n+k]*B[k*n+j];
            }
        }
    }
}

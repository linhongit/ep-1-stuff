#include <stdlib.h>

#include "bubblesort.h"

void bubblesort(int n, int* arr) {
    //i = n-1,..,0
    for (int i = n-1; i>=0; i--){
        //j = 1,...,i
        for (int j = 1; j<=i; j++){
            if (arr[j-1]>arr[j]){
                //swap
                int temp = arr[j];
                arr[j] = arr[j - 1];
                arr[j - 1] = temp;
            }
        }
    }
}
